unit crud.model.entidade.pessoa;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('PESSOA', '')]
  [PrimaryKey('CODIGO', NotInc, NoSort, False, 'Chave prim�ria')]
  TPESSOA = class
  private
    { Private declarations } 
    FCODIGO: Integer;
    FNOME: Nullable<String>;
    FTELEFONE: Nullable<String>;
    FCNPJCFP: Nullable<String>;
    FDATACADASTR: Nullable<TDateTime>;
  public 
    { Public declarations } 
    [Restrictions([NotNull])]
    [Column('CODIGO', ftInteger)]
    [Dictionary('CODIGO', 'Mensagem de valida��o', '', '', '', taCenter)]
    property codigo: Integer read FCODIGO write FCODIGO;

    [Column('NOME', ftString, 60)]
    [Dictionary('NOME', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property nome: Nullable<String> read FNOME write FNOME;

    [Column('TELEFONE', ftString, 15)]
    [Dictionary('TELEFONE', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property telefone: Nullable<String> read FTELEFONE write FTELEFONE;

    [Column('CNPJCFP', ftString, 18)]
    [Dictionary('CNPJCFP', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property cnpjcfp: Nullable<String> read FCNPJCFP write FCNPJCFP;

    [Column('DATACADASTR', ftDateTime)]
    [Dictionary('DATACADASTR', 'Mensagem de valida��o', '', '', '', taCenter)]
    property datacadastr: Nullable<TDateTime> read FDATACADASTR write FDATACADASTR;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TPESSOA)

end.
