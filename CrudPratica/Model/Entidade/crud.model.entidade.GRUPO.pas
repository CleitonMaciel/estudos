unit crud.model.entidade.grupo;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('GRUPO', '')]
  [PrimaryKey('CODIGO', NotInc, NoSort, False, 'Chave prim�ria')]
  TGRUPO = class
  private
    { Private declarations } 
    FCODIGO: Integer;
    FDESCRICA: Nullable<String>;
  public 
    { Public declarations } 
    [Restrictions([NotNull])]
    [Column('CODIGO', ftInteger)]
    [Dictionary('CODIGO', 'Mensagem de valida��o', '', '', '', taCenter)]
    property codigo: Integer read FCODIGO write FCODIGO;

    [Column('DESCRICA', ftString, 30)]
    [Dictionary('DESCRICA', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property descrica: Nullable<String> read FDESCRICA write FDESCRICA;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TGRUPO)

end.
