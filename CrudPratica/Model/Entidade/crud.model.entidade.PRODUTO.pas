unit crud.model.entidade.produto;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('PRODUTO', '')]
  [PrimaryKey('CODIGO', NotInc, NoSort, False, 'Chave prim�ria')]
  TPRODUTO = class
  private
    { Private declarations } 
    FCODIGO: Integer;
    FDESCRICAO: Nullable<String>;
    FPRECO: Nullable<Double>;
    FGRUPO_CODIGO: Nullable<Integer>;
    FDATACADASTRO: Nullable<TDateTime>;
  public 
    { Public declarations } 
    [Restrictions([NotNull])]
    [Column('CODIGO', ftInteger)]
    [Dictionary('CODIGO', 'Mensagem de valida��o', '', '', '', taCenter)]
    property codigo: Integer read FCODIGO write FCODIGO;

    [Column('DESCRICAO', ftString, 60)]
    [Dictionary('DESCRICAO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property descricao: Nullable<String> read FDESCRICAO write FDESCRICAO;

    [Column('PRECO', ftBCD, 18, 4)]
    [Dictionary('PRECO', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property preco: Nullable<Double> read FPRECO write FPRECO;

    [Column('GRUPO_CODIGO', ftInteger)]
    [Dictionary('GRUPO_CODIGO', 'Mensagem de valida��o', '', '', '', taCenter)]
    property grupo_codigo: Nullable<Integer> read FGRUPO_CODIGO write FGRUPO_CODIGO;

    [Column('DATACADASTRO', ftDateTime)]
    [Dictionary('DATACADASTRO', 'Mensagem de valida��o', '', '', '', taCenter)]
    property datacadastro: Nullable<TDateTime> read FDATACADASTRO write FDATACADASTRO;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TPRODUTO)

end.
