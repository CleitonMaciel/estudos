program prjCrud;

uses
  Vcl.Forms,
  Crud.View.Principal in 'View\Crud.View.Principal.pas' {Form1},
  Crud.Model.Interfaces in 'Model\Crud.Model.Interfaces.pas',
  crud.model.entidade.GRUPO in 'Model\Entidade\crud.model.entidade.GRUPO.pas',
  crud.model.entidade.PESSOA in 'Model\Entidade\crud.model.entidade.PESSOA.pas',
  crud.model.entidade.PRODUTO in 'Model\Entidade\crud.model.entidade.PRODUTO.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
